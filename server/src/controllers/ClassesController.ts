import { Request, Response } from 'express';
import convertHoursToMinutes from "../utils/convertHoursToMinutes";
import db from '../database/connection';
interface ScheduleItem { week_day: number, from: string, to: string }

export default class ClassesController {
  async index(req: Request, res: Response) {
    const filters = req.query;
    const subject = filters.subject as string;
    const week_day = filters.week_day as string;
    const time = filters.time as string;

    try {
      if(!filters.week_day || !filters.subject || !filters.time) {
        return res.status(400).json({
          "error": "error"
        })
      }

      const timeInMinutes = convertHoursToMinutes(time);

      const classes = await db('classes')
        .whereExists(function() {
          this.select('class_schedule.*')
          .from('class_schedule')
          .whereRaw('`class_schedule`.`class_id`  = `classes`.`id`')
          .whereRaw('`class_schedule`.`week_day` = ??', [Number(week_day)])
          .whereRaw('`class_schedule`.`from` <= ??', [timeInMinutes])
          .whereRaw('`class_schedule`.`to` > ??', [timeInMinutes])
        })
        .where('classes.subject', '=', subject)
        .join('users', 'classes.user_id', '=', 'users.id')
        .select(['classes.*', 'users.*']);
      return res.json({classes});
    } catch(err) {
      return res.status(400).send({"error": err});
    }
  }

  async create (req: Request, res: Response) {
    const { name, whatsapp, bio, subject, cost, schedule } = req.body;
    const avatar = 'photo'
    const trx = await db.transaction();
    
    try{
      const insertUsersIds = await trx('users').insert({
        name, avatar, whatsapp, bio
      });

      const user_id = insertUsersIds[0];
    
      const insertedClassedId = await trx('classes').insert({
        subject, cost, user_id
      });
    
      const class_id = insertedClassedId[0];
    
      const classSchedule = schedule.map((scheduleItem: ScheduleItem) => {
          
          return {
            class_id,
            week_day: scheduleItem.week_day,
            from: convertHoursToMinutes(scheduleItem.from),
            to: convertHoursToMinutes(scheduleItem.to)
          };
        });
    
        await trx('class_schedule').insert(classSchedule);
    
        await trx.commit();
    
        return res.status(201).send();
      } catch (err) {
        await trx.rollback();
        return res.status(400).json({"error": err})
      }
  }
}