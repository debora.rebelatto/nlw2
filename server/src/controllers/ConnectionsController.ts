import {Request, Response} from 'express';

import db from '../database/connection';

export default class ConnectionsController {
  async index(req: Request, res: Response) {
    try{
      const totalConnections = await db('connections').count('* as total');
      const { total } = totalConnections[0];
      return res.json({ total });
    } catch (err) {
      return res.status(400).send({ error: 'error' });
    }
  }

  async create(req: Request, res: Response) {
    const { user_id } = req.body;
    await db('connections').insert({user_id});
    return res.status(201).json({ok: 'ok'})
  }
}
