import React from 'react';

import './style.css';

import logoImg from '../../assets/images/logo.svg';
import landingImg from '../../assets/images/landing.svg';

import studyIcon from '../../assets/icons/study.svg';
import giveClassesIcon from '../../assets/icons/give-classes.svg';
import purpleHeart from '../../assets/icons/purple-heart.svg';

function Landing () {
  return (
    <div id="page-landing">
      <div id="page-landing-content" className="container">
        <div className="logo-containter">
          <img src={logoImg} alt="proffy"/>
          <h2>Sua plataforma de estudos</h2>
        </div>
        <img src={landingImg} alt='' className="hero-image"/>
        <div className="buttons-container">
          <a href='' className="study">
            <img src={studyIcon} alt="Estudar"/>
            Estudar
          </a>
        </div>
        <div className="buttons-container">
          <a href="" className="study">
            <img src={giveClassesIcon} alt=""/>
            Estudar
          </a>
        </div>
        <span className="total-connections">
          Total de conexões
          <img src={purpleHeart} alt="PurpleHeart"/>
        </span>
      </div>
    </div>
  );
}

export default Landing;